// Node modules
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
// Local components & modules
import { DirectoryComponent } from "./directory.component";
import { DirectoryService } from "./services/directory.service";
import { SharedModule } from "../shared/shared.module";

@NgModule({
  imports: [CommonModule, SharedModule],
  declarations: [DirectoryComponent],
  providers: [DirectoryService, NgbModal]
})
export class DirectoryModule {}
