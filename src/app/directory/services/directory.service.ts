// Node modules
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { BASE_URL } from "../../shared/constants/backend";

@Injectable()
export class DirectoryService {
  constructor(private http: HttpClient) {}

  getMembers(page_number:number,page_size:number) {
    return this.http.get(
      `${BASE_URL}/api/v1/members/directory?page=${page_number}&page_size=${page_size}`
    );
  }
}