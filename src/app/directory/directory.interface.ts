export interface IDirectoryMember {
  count;
  next;
  previous;
  results: [
    {
      id;
      first_name;
      last_name;
      title;
      profile_picture_url;
      company: {
        id;
        name;
      };
      skills: [
        {
          key;
          name;
        }
      ];
      location: {
        country_code;
        city_code;
        site_code;
        floor;
        asset_code;
        asset_number;
      };
    }
  ];
}
