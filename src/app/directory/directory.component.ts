// Node modules
import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import * as _ from "lodash";

// Local components & modules
import { FormControl, FormBuilder, FormGroup } from "@angular/forms";
import { DirectoryService } from "./services/directory.service";
import { IDirectoryMember } from "./directory.interface";

@Component({
  selector: "app-directory",
  templateUrl: "./directory.component.html",
  styleUrls: ["./directory.component.scss"],
  providers: [DirectoryService]
})
export class DirectoryComponent implements OnInit {
  private _members: IDirectoryMember["results"];
  members: IDirectoryMember["results"] = null;

  pageSize: number = 10;
  pageNumber: number = 1;
  collectionSize: number = 0;
  searchInput: string = "";
  constructor(
    private directoryService: DirectoryService,
    private cd: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.getMembers(this.pageNumber, this.pageSize);
  }
  ngAfterViewInit() {
    this.cd.detectChanges();
  }
  /********* PUBLIC *********/
  public getPageData(event) {
    this.searchInput ='';
    this.pageNumber = event;
    this.getMembers(this.pageNumber, this.pageSize);
  }

  public typeSelected(type: string) {
    if (type === "Members") {
      this.members = <IDirectoryMember["results"]>_.sortBy(this.members, "id");
    } else if (type === "Companies") {
      this.members = <IDirectoryMember["results"]>(
        _.sortBy(this.members, ["company.id", "id"])
      );
    }
  }

  public search() {
    let text = this.searchInput;
    if (text !== null && text !== undefined && text !== "") {
      this.members = this._members;
      this.members = <IDirectoryMember["results"]>_.filter(
        this.members,
        (member: IDirectoryMember["results"][0]) => {
          let fullName = `${member.first_name} ${member.last_name}`;
          return (
            member.id.toString().indexOf(text) > -1 ||
            fullName.toLowerCase().indexOf(text) > -1 ||
            member.company.id.toString().indexOf(text) > -1 ||
            member.company.name.toLowerCase().indexOf(text) > -1
          );
        }
      );
    } else {
      this.members = this._members;
    }
  }

  /********* PRIVATE *********/
  private getMembers(pageNumber: number, pageSize: number) {
    this.directoryService.getMembers(this.pageNumber, this.pageSize).subscribe(
      (data: IDirectoryMember) => {
        if (data && data.results) {
          this.members = data.results;
          this._members = data.results;
          this.collectionSize = data.count;
          this.typeSelected("Members");
        } else {
          /**
           * TODO LOGGER SERVICE
           */
          console.log("getMembers no data", data);
        }
      },
      err => console.error(err)
    );
  }


}
