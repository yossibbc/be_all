// Node modules
import {NgModule} from "@angular/core";
// Local components & modules
import {UrlPipe} from "./url.pipe";
import {NewlinePipe} from "./new-line.pipe";
import { CityNamePipe } from "./city-name.pipe";

@NgModule({
  declarations: [
    UrlPipe,
    NewlinePipe,
    CityNamePipe
  ],
  exports: [
    UrlPipe,
    NewlinePipe,
    CityNamePipe
  ]
})

export class PipeModule {}
