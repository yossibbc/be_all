// Node modules
import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "cityname"
})
export class CityNamePipe implements PipeTransform {
  transform(value): string {
    switch (value) {
      case "TLV":
        return "Tel Aviv";
      case "GVT":
        return "Givatayim";
      default:
        return "";
    }
  }
}
