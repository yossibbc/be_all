// Node modules
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { TranslateModule } from "@ngx-translate/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MomentModule } from "angular2-moment";

// Local components & modules
import { SpinnerComponent } from "./spinner/spinner.component";
import { PipeModule } from "./pipes/pipe.module";
import { PostPlaceHolderComponent } from "./post-place-holder/post-place-holder.component";
import { MembersListModalComponent } from "./members-list/members-list-modal/members-list-modal.component";
import { MembersListComponent } from "./members-list/members-list.component";
import { FocusDirective } from "./directives/focus.directive";
import { AutoSizeDirective } from "./directives/auto-size.directive";
import { SkillsListModalComponent } from "./skills-list/skills-list-modal/skills-list-modal.component";
import { SkillsListComponent } from "./skills-list/skills-list.component";
import { SearchBoxComponent } from "./search-box/search-box.component";

@NgModule({
  declarations: [
    SpinnerComponent,
    PostPlaceHolderComponent,
    MembersListComponent,
    MembersListModalComponent,
    SkillsListComponent,
    SkillsListModalComponent,
    FocusDirective,
    AutoSizeDirective,
    SearchBoxComponent
    
  ], 
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MomentModule,
    NgbModule,
    TranslateModule,
    RouterModule,
    PipeModule
  ],
  exports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MomentModule,
    NgbModule,
    TranslateModule,
    RouterModule,
    SpinnerComponent,
    PostPlaceHolderComponent,
    MembersListComponent,
    PipeModule,
    FocusDirective,
    AutoSizeDirective,
    SkillsListComponent,
    SearchBoxComponent
  ],
  entryComponents: [MembersListModalComponent, SkillsListModalComponent]
})
export class SharedModule {}
