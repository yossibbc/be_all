// Node modules
import { Component, OnInit, Input } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
@Component({
  selector: "ba-skills-list-modal",
  templateUrl: "skills-list-modal.component.html",
  styleUrls: ["skills-list-modal.component.scss"]
})
export class SkillsListModalComponent implements OnInit {
  skills: ISkillsListModal[];

  @Input() title;
  @Input()
  set setSkills(skills) {
    if (skills) {
      // Set local param of skills
      this.skills = skills;
    }
  }

  constructor(public activeModal: NgbActiveModal) {}

  ngOnInit() {}
}
