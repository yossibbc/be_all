// Node modules
import { Component, OnInit, Input } from "@angular/core";
import { NgbModal, NgbTooltipConfig } from "@ng-bootstrap/ng-bootstrap";
// Local components & modules
import { SkillsListModalComponent } from "./skills-list-modal/skills-list-modal.component";
import { ISkills } from "../interfaces";
import { IDisplayedSkill } from "./skills-list.interface";

@Component({
  selector: "ba-skills-list",
  templateUrl: "skills-list.component.html",
  styleUrls: ["skills-list.component.scss"]
})
export class SkillsListComponent implements OnInit {
  // Local vars
  isMoreSkillsThanMax: boolean = false;
  displayedSkills: IDisplayedSkill[] = [];
  numOfSkills: number;
  displayTitle: boolean = false;
  skills: ISkills[];

  // Input params from consuming components
  @Input() text: string;
  @Input() maxDisplayedSkills: number;

  @Input()
  set setSkills(Skills) {
    if (Skills) {
      // Set skills list
      this.skills = Skills;

      // We need parameter numOfSkills because the template returns "Cannot read property 'length' of undefined"
      this.numOfSkills = this.skills.length;

      // Indicate whether we have more skills than we should show on screen, and set the array of displayed skills
      if (this.skills.length > this.maxDisplayedSkills) {
        this.isMoreSkillsThanMax = true;
        // Set array of (maxDisplayedSkills-1) skills to display + 1 avatar with number of remaining skills
        this.displayedSkills = this.setDisplayedSkillsArray(
          this.skills,
          this.maxDisplayedSkills - 1
        );
      } else {
        // Set displayed skills array of all skills
        this.displayedSkills = this.setDisplayedSkillsArray(
          this.skills,
          this.skills.length
        );
      }
    }
  }

  constructor(
    private modalService: NgbModal,
    private TTConfig: NgbTooltipConfig
  ) {
    TTConfig.placement = "bottom";
    TTConfig.triggers = "hover";
  }

  ngOnInit() {}

  private setDisplayedSkillsArray(skills, numOfSkills) {
    let displayedSkills: IDisplayedSkill[] = [];

    for (let i = 0; i < numOfSkills; i++) {
      displayedSkills.push({
        name: skills[i].name
      });
    }
    return displayedSkills;
  }

  showAllSkills() {
    let skillsList: ISkillsListModal[] = [];
    const skillsModal = this.modalService.open(SkillsListModalComponent);

    // Set list of skills at modal format
    for (let i = 0; i < this.skills.length; i++) {
      skillsList.push({
        name: this.skills[i].name
      });
    }
    skillsModal.componentInstance.setSkills = skillsList;

    // Set modal title
    skillsModal.componentInstance.title =
      this.skills.length + " " + (this.text ? this.text : "skills");
  }

  rightClickImage(event) {
    event.stopPropagation();
  }
}
