// Node modules
import {
  Component,
  Output,
  EventEmitter,
  Input,
  ChangeDetectorRef
} from "@angular/core";
import { FormGroup, FormControl, FormBuilder } from "@angular/forms";

// Local components & modules

@Component({
  selector: "ba-search-box",
  templateUrl: "./search-box.component.html",
  styleUrls: ["search-box.component.scss"]
})
export class SearchBoxComponent {
  selectedType: string;
  typesArray: Array<string>;
  searchForm: FormGroup;
  searchModel: FormControl;
  mobileFilter:string ='';
  @Output("userSelectType") userSelectType = new EventEmitter<string>();
  @Output("searchPressEnter") searchPressEnter = new EventEmitter();
  @Input()
  get searchInput() {
    return this.searchForm.controls["searchModel"].value;
  }
  @Output() searchInputChange = new EventEmitter();

  set searchInput(value) {
    this.searchForm.controls["searchModel"].setValue(value);
    this.searchInputChange.emit(value);
  }

  constructor(private fb: FormBuilder) {
    this.typesArray = ["Members", "Companies"];
    this.selectedType = "Members";
    this.searchForm = this.fb.group({
      searchModel: [""]
    });
  }

  ngOnInit() {
    this.searchInput;
    this.listenToSearchInput();
  }

  ngOnDestroy() {}

  /***** PUBLIC *****/

  typeSelected(type: string) {
    this.userSelectType.emit(type);
  }

  searchBoxEnter() {
    this.searchInput = this.searchForm.controls["searchModel"].value;
    this.searchPressEnter.emit();
  }

  userPressOnIcon(type: string) {
    this.mobileFilter = type;
  }

  /***** PRIVATE *****/

  private listenToSearchInput() {
    this.searchForm.controls["searchModel"].valueChanges
      .debounceTime(1500)
      .subscribe(query => {
        this.searchInput = query;
        this.searchBoxEnter();
      });
  }
}
