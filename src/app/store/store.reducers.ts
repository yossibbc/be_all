// Node modules
import { composeReducers, defaultFormReducer } from "@angular-redux/form";
import { combineReducers } from "redux";
// Local components & modules
import { MemberReducer } from "../member/redux/member.reducer";
import { CompanyReducer } from "../company/redux/company.reducer";
import { UserReducer } from "../user/redux/user.reducer";

export const rootReducer = composeReducers(
  defaultFormReducer(),
  combineReducers({
    user: UserReducer,
    member: MemberReducer,
    company: CompanyReducer
  })
);
