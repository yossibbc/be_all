// Node modules
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { NgReduxRouterModule } from "@angular-redux/router";
import { NgReduxModule } from "@angular-redux/store";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { TranslateLoader, TranslateModule } from "@ngx-translate/core";
// Local components & modules
import { AppComponent } from "./app.component";
import { MemberModule } from "./member/member.module";
import { CompanyModule } from "./company/company.module";
import { appRoutes } from "./routes";
import { StoreModule } from "./store/store.module";
import { UserActions } from "./user/redux/user.actions";
import { SidebarComponent } from "./sidebar/sidebar.component";
import { DirectoryModule } from "./directory/directory.module";

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [AppComponent, SidebarComponent],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule.forRoot(),
    NgReduxModule,
    NgReduxRouterModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    StoreModule,
    DirectoryModule,
    MemberModule,
    CompanyModule
  ],
  providers: [UserActions],
  bootstrap: [AppComponent]
})
export class AppModule {}
